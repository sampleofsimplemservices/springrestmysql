package com.example.SpringRestMySQL.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.SpringRestMySQL.model.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Long>{

	List<Customer> findByAge(int age);
	
}
